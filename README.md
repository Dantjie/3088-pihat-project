# 3088 PiHat Project
Our microHAT is an Uninterrupted power supply (UPS) for powering a Raspberry Pi Zero. The UPS provides backup power when the regular power source fails. There are several scenarios in which the UPS can be used. The UPS is only attached to the Pi Zero and AC power.

Use cases:
The UPS can serve as a safety measure allowing for proper shutting down of the Pi Zero and its daughter boards.
The UPS can serve as a backup power source for powering the Pi Zero during load shedding for a duration of 2 hours and 30 minutes.
The UPS can allow for portability in situations where the Pi Zero has to be mobile.

contents of DC stepdown circuit:
DC Stepdown
Input:
If mains in use: 12V from AC/DC rectifier.
If battery in use: 7.2V from battery 
Contents:
Cannot receive more than 18V input supply, thus the AC-DC needs an external step down transformer.
Contains LM7805 DC step down chip (to step down the DC voltage to usable Pi levels).
Contains the maintenance switch (to switch off power to the Raspberry Pi when the UPS needs to be maintained).
Capacitors (to protect the LM7805 chip).
Diode (to prevent a short circuit when using battery power).
Output to our comparator op-amp to switch to the battery power.
Input from the battery or AC-DC circuit, depending on which is in use.
Output is used to power the Raspberry Pi.
Output:
Between (3.3V-5V) to the Pi zero.

